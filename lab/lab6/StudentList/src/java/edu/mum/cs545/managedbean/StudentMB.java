package edu.mum.cs545.managedbean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.mum.cs545.bean.Student;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author sam
 */
@Named(value = "studentMB")
@ApplicationScoped
public class StudentMB implements Serializable {

    private TreeMap<Integer, Student> students = new TreeMap<>();
    private Student student = new Student();
    private List<String> ownsList = new ArrayList<>();
    private Map<String, String> laptops = new HashMap<>();

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Map<String, String> getLaptops() {
        return laptops;
    }

    /**
     * Creates a new instance of StudentMB
     */
    public StudentMB() {
        initOwnList();
        initLaptop();
    }

    public void initOwnList() {
        ownsList.add("NONE");
        ownsList.add("Car");
        ownsList.add("Bike");
        ownsList.add("Book");
    }

    public void initLaptop() {
        laptops.put("Asus", "Asus");
        laptops.put("Dell", "Dell");
        laptops.put("Mac", "Mac");
        laptops.put("HP", "HP");
    }

    public List<String> getOwnsList() {
        return ownsList;
    }

    public void setStudents(TreeMap<Integer, Student> students) {
        this.students = students;
    }

    public TreeMap<Integer, Student> getStudents() {
        return students;
    }

    public String addStudent() {
        int studentID = 0;
        try {
            studentID = students.lastKey();
        } catch (NoSuchElementException e) {

        }
        student.setId(++studentID);
        students.put(studentID, student);
        return "index?faces-redirect=true";
    }

    public String saveStudent() {
        students.put(student.getId(), student);
        return "view?faces-redirect=true";
    }

    public List<Student> getStudentList() {
        return new ArrayList<>(students.values());
    }

    public String addNewStudent() {
        student = new Student();
        return "add?faces-redirect=true";
    }

    public String editStudent(int id) {
        student = students.get(id);
        return "edit?faces-redirect=true";
    }

    public String deleteStudent(int id) {
        students.remove(id);
        return "view?faces-redirect=true";
    }

    public String resetStudent(boolean isEdit) {
        student=new Student();
        student.setName("");
        student.setGpa(0);
        student.setHasPhone(false);
        student.setOwn("NONE");
        student.setLaptop("");
        if (isEdit) {
            return "edit?faces-redirect=true";
        } else {
            return "add?faces-redirect=true";
        }
    }
}
