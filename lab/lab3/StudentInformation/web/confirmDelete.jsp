<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Confirm Delete</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Student Information:</h1>
        <form action="deleteProcess" method="post">
        <table>
            <tr>
                <td>ID:</td>
                <td>${student.id}</td>
            </tr>
            <tr>
                <td>Name:</td>
                <td>${student.name}</td>
            </tr>
            <tr>
                <td>GPA:</td>
                <td>${student.gpa/10}</td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td style="text-transform: capitalize">${student.gender}</td>
            </tr>
        </table>
            <input type="hidden" name="id" value="${student.id}">
            <input type="submit" value="Delete" name="action">
            <input type="submit" value="Cancel" name="action">
        </form>
    </body>
</html>
