<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>View Student</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Student Information:</h1>
        <form action="index.jsp" method="get">
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>GPA</th>
                    <th>Gender</th>
                </tr>
                <c:forEach items="${students}" var="student">
                    <tr>
                        <td><c:out value="${student.id}" /></td>
                        <td><c:out value="${student.name}" /></td>
                        <td><c:out value="${student.gpa/10}" /></td>
                        <td style="text-transform: capitalize"><c:out value="${student.gender}" /></td>
                    </tr>
                </c:forEach>
            </table>
            <br>
            <input type="submit" value="Done" name="action">
        </form>
    </body>
</html>
