<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Delete Student</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Enter the id to delete:</h1>
        <form action="deleteProcess" method="get">
            <input type="text" name="id">
            <input type="submit" value="Find" name="action">
            <input type="submit" value="Cancel" name="action">
        </form>
        <div style="color:red">${error}</div>
    </body>
</html>
