<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Add Student</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Student Information:</h1>
        <form action="add" method="post">
        <table>
            <tr>
                <td>ID:</td>
                <td><input type="text" name="id"></td>
            </tr>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="studentName"></td>
            </tr>
            <tr>
                <td>GPA:</td>
                <td><input type="text" name="gpa"></td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td>
                    <input type="radio" name="gender" value="male" checked="checked"> Male
                    <input type="radio" name="gender" value="female"> Female<br>
                </td>
            </tr>
        </table>
            <input type="submit" value="Add" name="action">
            <input type="submit" value="Cancel" name="action">
            <input type="reset" />
        </form>
        <div style="color:red">${error}</div>
    </body>
</html>
