/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.servlet;

import edu.mum.cs545.model.Student;
import edu.mum.cs545.util.ViewUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 984897
 */
@WebServlet(name = "AddStudent", urlPatterns = {"/add"})
public class AddStudent extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        ViewUtil.returnView(request, response, "add");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action.equals("Add")) {
            try {
//                int id = validateID(request);
//                String name = validateName(request);
//                int gpa = validateGPA(request);
//                String gender = request.getParameter("gender");
//                Student student = new Student(id, name, gpa, gender);
                Student student = studentValidator(request);
                List<Student> students;
                students = (List<Student>) this.getServletContext().getAttribute("students");
                if (students == null) {
                    students = new ArrayList<>();
                }
                students.add(student);
                this.getServletContext().setAttribute("students", students);
            } catch (Exception e) {
                request.setAttribute("error", "Validation Error:<br/>" + e.getLocalizedMessage());
                ViewUtil.returnView(request, response, "add");
            }
        }
        ViewUtil.returnView(request, response, "index");
        //processRequest(request, response);
    }

    private int validateID(HttpServletRequest request) throws Exception {
        String idStr = request.getParameter("id");
        if (idStr == null || idStr.isEmpty()) {
            throw new Exception("ID is requied!");
        }
        int id = 0;
        try {
            id = Integer.parseInt(idStr);
        } catch (Exception e) {
            throw new Exception("ID need to be integer!");
        }
        return id;
    }

    private int validateGPA(HttpServletRequest request) throws Exception {
        int gpa = 0;
        try {
            gpa = Integer.parseInt(request.getParameter("gpa"));
        } catch (Exception e) {
            throw new Exception("GAP need to be integer!");
        }
        if (gpa < 0 || gpa > 40) {
            throw new Exception("GPA need to be in 00-40");
        }
        return gpa;
    }

    private String validateName(HttpServletRequest request) throws Exception {
        String name = request.getParameter("studentName");
        if (name == null || name.isEmpty()) {
            throw new Exception("Name is requied!");
        }
        return name;
    }

    private Student studentValidator(HttpServletRequest request) throws Exception {
        String idStr = request.getParameter("id");
        StringBuilder error = new StringBuilder();
        int id = 0;
        if (idStr == null || idStr.isEmpty()) {
            error.append("ID is requied!<br/>");
        } else {
            id = 0;
            try {
                id = Integer.parseInt(idStr);
            } catch (Exception e) {
                error.append("ID need to be integer!<br/>");
            }
        }
        String name = request.getParameter("studentName");
        if (name == null || name.isEmpty()) {
            error.append("Name is requied!<br/>");
        }
        int gpa = 0;
        try {
            gpa = Integer.parseInt(request.getParameter("gpa"));
        } catch (Exception e) {
            error.append("GPA need to be integer!<br/>");
        }
        if (gpa < 0 || gpa > 40) {
            error.append("GPA need to be in 00-40<br/>");
        }

        String gender = request.getParameter("gender");
        if (error.toString().length() > 0) {
            throw new Exception(error.toString());
        }
        Student student = new Student(id, name, gpa, gender);
        return student;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
