/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.servlet;

import edu.mum.cs545.model.Student;
import edu.mum.cs545.util.ViewUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 984897
 */
@WebServlet(name = "ProcessDelete", urlPatterns = {"/deleteProcess"})
public class ProcessDelete extends HttpServlet {
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action.equals("Find")){
            int id=0;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (Exception e) {
            }
            List<Student> students;
            students = (List<Student>) this.getServletContext().getAttribute("students");
            if(students==null) students=new ArrayList<>();
            Student student=findStudentById(students, id);
            if(student!=null){
                request.setAttribute("student", student);
                ViewUtil.returnView(request, response, "confirmDelete");
            }else{
                request.setAttribute("error", "Student Not Found!");
                ViewUtil.returnView(request, response, "delete");
            }
        }else{
            ViewUtil.returnView(request, response, "index");
        }
    }

    private Student findStudentById(List<Student> students,int id){
        Optional<Student> student=students.stream().filter(s->s.getId()==id).findFirst();
        return student.isPresent()?student.get():null;
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action.equals("Delete")) {
            int id = Integer.parseInt(request.getParameter("id"));        
            List<Student> students = (List<Student>) this.getServletContext().getAttribute("students");
            for (Student student : students) {
                if(student.getId()==id){
                    students.remove(student);
                    break;
                }
            }
            this.getServletContext().setAttribute("students", students);
        }
        ViewUtil.returnView(request, response, "index");
        
    }
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
