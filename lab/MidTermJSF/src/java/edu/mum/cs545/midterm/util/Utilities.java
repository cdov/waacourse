/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;

/**
 *
 * @author sam
 */
public class Utilities {
    public static String getResourceBundleString(
            String resourceBundleName,
            String resourceBundleKey)
        throws MissingResourceException {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ResourceBundle bundle = 
        facesContext.getApplication().getResourceBundle(
            facesContext, resourceBundleName);
    return bundle.getString(resourceBundleKey);
}
}
