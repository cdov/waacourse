/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.managedbean;

import edu.mum.cs545.midterm.bean.LoginBean;
import edu.mum.cs545.midterm.model.Login;
import edu.mum.cs545.midterm.model.Person;
import edu.mum.cs545.midterm.util.Utilities;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author sam
 */
@Named(value = "loginMB")
@SessionScoped
public class LoginMB implements Serializable {

    @Inject
    private LoginBean loginBean;

    @Inject
    private ApplicationMB applicationMB;
    
    private Person loginUser;


    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Person getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Person loginUser) {
        this.loginUser = loginUser;
    }

    /**
     * Creates a new instance of LoginMB
     */
    public LoginMB() {
    }

    public String login() {
        Login login = new Login();
        loginUser = login.login(loginBean.getUsername(), loginBean.getPassword());
        if (loginUser == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage(Utilities.getResourceBundleString("msgs", "loginError")));            
            return "login";
        } else {
            applicationMB.setSessionCount(applicationMB.getSessionCount()+1);
            return "home?faces-redirect=true";
        }
    }
    public String logout(){
        loginUser=null;
        applicationMB.setSessionCount(applicationMB.getSessionCount()-1);
        return "index?faces-redirect=true";
    }
}
