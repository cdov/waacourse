/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sam
 */
public class Login {

    private List<Person> people = new ArrayList<>();

    public Login(){
        createAccounts();
    }
    
    // login return person object instead of boolean use to store login user
    public Person login(String username, String password) {
        for (Person person : people) {
            if(person.getUserName().equalsIgnoreCase(username) && 
               person.getPassword().equals(password))
                return person;
        }
        return null;
    }    

    public List<Person> getAccounts() {
        return people;
    }

    public boolean createAccounts() {
        people = new ArrayList<>();
        people.add(new Person("Jack", "jack", "apple", true));
        people.add(new Person("Jill", "jill", "rose", false));
        people.add(new Person("John", "john", "car", false));
        people.add(new Person("Jim", "jim", "house", false));
        people.add(new Person("Jane", "jane", "diamond", false));
        people.add(new Person("Jordan", "jo", "ball", false));
        people.add(new Person("Jasmin", "jas", "jazz", false));
        people.add(new Person("Sam", "sammy", "123456", true));
        return true;
    }
}
