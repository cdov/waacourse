/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.managedbean;

import edu.mum.cs545.midterm.bean.ProductBean;
import edu.mum.cs545.midterm.model.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 *
 * @author sam
 */
@Named(value = "productMB")
@ApplicationScoped
public class ProductMB {
    @Inject
    private ProductBean productBean;
    private TreeMap<Integer, Product> products = new TreeMap<>();

    public ProductBean getProductBean() {
        return productBean;
    }

    public void setProductBean(ProductBean productBean) {
        this.productBean = productBean;
    }

    public TreeMap<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(TreeMap<Integer, Product> product) {
        this.products = product;
    }
    public List<Product> getProductList(){
        if(products.size()>0)
            return new ArrayList<>(products.values());
        else
            return new ArrayList<>();
    }
    /**
     * Creates a new instance of ProductMB
     */
    public ProductMB() {
    }
    
    public String addProduct() {
        int productId = 0;
        try {
            productId = products.lastKey();
        } catch (NoSuchElementException e) {

        }
        Product product=new Product(++productId,productBean.getName(),productBean.getPrice(),productBean.getDescription());
        products.put(productId, product);
        return "home?faces-redirect=true";
    }
    public String editProduct(int id) {
        Product product=products.get(id);
        productBean.setId(product.getId());
        productBean.setName(product.getName());
        productBean.setPrice(product.getPrice());
        productBean.setDescription(product.getDescription());
        return "edit?faces-redirect=true";
    }

    public String saveProduct() {
        products.put(productBean.getId(), new Product(productBean.getId(),productBean.getName(),productBean.getPrice(),productBean.getDescription()));
        return "view?faces-redirect=true";
    }
    
    public String deleteProduct(int id) {
        products.remove(id);
        return "view?faces-redirect=true";
    }
    
    public void clearProductBean(){
        productBean.setId(0);
        productBean.setName("");
        productBean.setPrice(0);
        productBean.setDescription("");
    }
    public String newProductPage(){
        clearProductBean();
        return "add?faces-redirect=true";
    }
}
