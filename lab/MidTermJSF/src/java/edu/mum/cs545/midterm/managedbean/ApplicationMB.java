/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.managedbean;

import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author sam
 */
@Named(value = "applicationMB")
@ApplicationScoped
public class ApplicationMB {
    private int sessionCount=0;

    public int getSessionCount() {
        return sessionCount;
    }

    public void setSessionCount(int sessionCount) {
        this.sessionCount = sessionCount;
    }
    /**
     * Creates a new instance of ApplicationMB
     */
    
    public ApplicationMB() {
    }
    
}
