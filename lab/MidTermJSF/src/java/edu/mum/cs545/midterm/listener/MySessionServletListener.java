/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.listener;

import edu.mum.cs545.midterm.managedbean.ApplicationMB;
import javax.inject.Inject;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author sam
 */
public class MySessionServletListener implements HttpSessionListener {
    @Inject
    private ApplicationMB applicationMB;
    
    @Override
    public void sessionCreated(HttpSessionEvent se) {
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        applicationMB.setSessionCount(applicationMB.getSessionCount()-1);
    }
}
