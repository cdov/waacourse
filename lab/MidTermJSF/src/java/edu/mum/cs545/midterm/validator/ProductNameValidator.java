/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.validator;

import edu.mum.cs545.midterm.managedbean.ProductMB;
import edu.mum.cs545.midterm.model.Product;
import edu.mum.cs545.midterm.util.Utilities;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author sam
 * using @Named instead of @FacesValidator because @FacesValidator doesn't support DI @Inject
 * http://stackoverflow.com/questions/7572335/how-to-inject-in-facesvalidator-with-ejb-persistencecontext-inject-autow/7572413#7572413
 */
@Named(value="productNameValidator")
@RequestScoped
public class ProductNameValidator implements Validator {

    @Inject
    private ProductMB productMB;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (productMB.getProductList() != null) {
            for (Product product : productMB.getProductList()) {
                if (product.getName().equalsIgnoreCase(value.toString())
                        && product.getId() != productMB.getProductBean().getId()) {
                    FacesMessage msg
                            = new FacesMessage("ProductName validation failed.",
                                    Utilities.getResourceBundleString("msgs", "productnameError"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(msg);
                }

            }
        }

    }

}
