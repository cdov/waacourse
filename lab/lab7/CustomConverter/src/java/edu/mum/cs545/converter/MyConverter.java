/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.converter;

import edu.mum.cs545.model.Cat;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author 984897
 */
@FacesConverter("myConverter")
public class MyConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String[] strs=value.split(",");
        int age=0;
        if(strs.length>=2){
            try {
                age=Integer.parseInt(strs[1]);
            } catch (Exception e) {
            }
        }
        Cat cat=new Cat(strs[0],age);
        return cat;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Cat cat=(Cat) value;
        if(cat.getName()==null && cat.getAge()==0) return "";
        return cat.toString();
    }
    
}
