package edu.mum.cs545.managedbean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.mum.cs545.model.Cat;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author 984897
 */
@Named(value = "converterMB")
@ApplicationScoped
public class ConverterMB {
    private Cat cat=new Cat();

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
    /**
     * Creates a new instance of ConverterMB
     */
    public ConverterMB() {
    }
    public String resultPage(){
        return "result?faces-redirect=true";
    }
    
    public String backPage(){
        cat=new Cat();
        return "index?faces-redirect=true";
    }
}
