<%-- 
    Document   : result
    Created on : May 24, 2016, 6:29:44 PM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Result</title>
    </head>
    <body>
        <h1>Result</h1>
        <p>${num1}${operator}${num2} = ${result}</p>
        <form action="index.jsp">
            <input type="submit" value="Back">
        </form>
    </body>
</html>
