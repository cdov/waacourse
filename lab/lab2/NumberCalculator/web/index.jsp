<%-- 
    Document   : index
    Created on : May 24, 2016, 4:49:49 PM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Number Calculator</title>
    </head>
    <body>
        <h1>Number Calculator</h1>
        <form action="NumberCalServlet" method="post">
            Enter 1st number:<input type="text" name="number1" /><br>
            Enter 2nd number:<input type="text" name="number2" /><br>
            <input type="submit" value="add" name="action"/>
            <input type="submit" value="substract" name="action"/>
        </form>
        <div style="color:red">${error}</div>
    </body>
</html>
