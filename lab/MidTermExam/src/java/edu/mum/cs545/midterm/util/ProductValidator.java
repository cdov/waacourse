/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.util;

import edu.mum.cs545.midterm.model.Product;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Validator;

/**
 *
 * @author sam
 */
public class ProductValidator {

    public static Product validate(HttpServletRequest request) {

        boolean hasError = false;
        String name = request.getParameter("productName");
        if (name == null || name.isEmpty()) {
            request.setAttribute("nameError", "Product must have a name");
            hasError = true;
        } else if (checkDuplicateProductName(request, name)) {
            request.setAttribute("nameError", "Product name can't be duplicate");
            hasError = true;
        }
        int price = 0;
        try {
            price = Integer.parseInt(request.getParameter("price"));
        } catch (Exception e) {
            request.setAttribute("priceError", "price must have a integer");
            hasError = true;
        }
        if (price < 1 || price > 850) {
            request.setAttribute("priceError", "price must be between $1-$850");
            hasError = true;
        }
        if (hasError) {
            return null;
        }
        String description = request.getParameter("description");
        Product product = new Product(name, price, description);
        return product;
    }

    public static boolean checkDuplicateProductName(HttpServletRequest request, String name) {
        List<Product> products = (List<Product>) request.getServletContext().getAttribute("products");
        int index=0;
        try {
            index = Integer.parseInt(request.getParameter("id"));
        } catch (Exception e) {
            index=-1;
        }
        
        for (int i=0;i<products.size();i++) {
            if (products.get(i).getName().equals(name) && i!=index-1) {
                return true;
            }
        }
        return false;
    }
}
