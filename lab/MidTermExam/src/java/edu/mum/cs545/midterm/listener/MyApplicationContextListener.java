/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.listener;

import edu.mum.cs545.midterm.model.Login;
import edu.mum.cs545.midterm.model.Product;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 *
 * @author sam
 */
public class MyApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        Login login=new Login();
//        login.createAccounts();
//        sce.getServletContext().setAttribute("people", login.getAccounts());
        List<Product> products;
        products = (List<Product>) sce.getServletContext().getAttribute("products");
        if (products == null) {
            products = new ArrayList<>();
        }
        sce.getServletContext().setAttribute("products", products);
        sce.getServletContext().setAttribute("sessionCount", 0);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
