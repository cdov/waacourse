/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author sam
 */
public class MySessionServletListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        int count=0;
        count=(int)se.getSession().getServletContext().getAttribute("sessionCount");
        count=count-1<=0?0:count--;
        se.getSession().getServletContext().setAttribute("sessionCount", count);
    }
}
