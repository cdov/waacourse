/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.servlet;

import edu.mum.cs545.midterm.model.Product;
import edu.mum.cs545.midterm.util.ProductValidator;
import edu.mum.cs545.midterm.util.ViewUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sam
 */
@WebServlet(name = "EditProductServlet", urlPatterns = {"/edit"})
public class EditProductServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        List<Product> products;
        products = (List<Product>) this.getServletContext().getAttribute("products");
        if (products == null) {
            products = new ArrayList<>();
        }
        if (products.size() > 0) {
            try {
                request.setAttribute("product", products.get(id - 1));
            } catch (Exception e) {
                ViewUtil.returnView(request, response, "404");
            }
        }
        request.setAttribute("id", id);
        RequestDispatcher view = request.getRequestDispatcher("edit.jsp?id=" + id);
        view.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        int id = Integer.parseInt(request.getParameter("id"));
        List<Product> products;
        products = (List<Product>) this.getServletContext().getAttribute("products");
        if (products == null) {
            products = new ArrayList<>();
        }
        if (action.equals("Modify")) {
            Product product = ProductValidator.validate(request);
            if (product == null) {
                request.setAttribute("id", id);
                request.setAttribute("product", products.get(id - 1));
                RequestDispatcher view = request.getRequestDispatcher("edit.jsp?id=" + id);
                view.forward(request, response);
                return;
            }

            products.set(id - 1, product);
            this.getServletContext().setAttribute("products", products);
        }
        ViewUtil.returnView(request, response, "view");
    }

    private Product productValidator(HttpServletRequest request) {
        boolean hasError = false;
        String name = request.getParameter("productName");
        if (name == null || name.isEmpty()) {
            request.setAttribute("nameError", "Product must have a name");
            hasError = true;
        }
        int price = 0;
        try {
            price = Integer.parseInt(request.getParameter("price"));
        } catch (Exception e) {
            request.setAttribute("priceError", "price must have a integer");
            hasError = true;
        }
        if (price < 1 || price > 850) {
            request.setAttribute("priceError", "price must be between $1-$850");
            hasError = true;
        }
        if (hasError) {
            return null;
        }
        String description = request.getParameter("description");
        Product product = new Product(name, price, description);
        return product;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
