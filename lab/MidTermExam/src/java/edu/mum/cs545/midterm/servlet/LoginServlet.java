/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.midterm.servlet;

import edu.mum.cs545.midterm.model.Login;
import edu.mum.cs545.midterm.model.Person;
import edu.mum.cs545.midterm.util.ViewUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sam
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username= request.getParameter("username");
        String password= request.getParameter("password");
        if(username==null || username.isEmpty() || password==null || password.isEmpty()){
            if(username==null || username.isEmpty()) request.setAttribute("userError", "Username must be entered");
            if(password==null || password.isEmpty()) request.setAttribute("passwordError", "Password must be entered");
            ViewUtil.returnView(request, response, "login");
        }
        Login login=new Login();
        Person loginUser=login.login(username, password);
        if(loginUser==null){
            request.setAttribute("loginError", "Invalid username and password");
            ViewUtil.returnView(request, response, "login");
        }
        request.getSession().setAttribute("loginUser", loginUser);
        int count=0;
        count=(int)this.getServletContext().getAttribute("sessionCount");
        this.getServletContext().setAttribute("sessionCount", count+1);
        ViewUtil.returnView(request, response, "home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
