<%-- 
    Document   : index
    Created on : Jun 4, 2016, 9:57:20 AM
    Author     : sam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mid-Term Exam</title>
    </head>
    <body>
        <div>
            <jsp:include page="templates/header.jsp" flush="true"/>
            Please select action to perform:<br/>
            
            <br/>
            <a href="view.jsp"><button>View List</button></a>
            <c:if test="${loginUser.admin==true}">
                <a href="add"><button>Add</button></a>
            </c:if>
            <jsp:include page="templates/footer.jsp" flush="true"/>
        </div>
    </body>
</html>
