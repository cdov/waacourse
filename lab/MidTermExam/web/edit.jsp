<%-- 
    Document   : index
    Created on : Jun 4, 2016, 9:57:20 AM
    Author     : sam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mid-Term Exam</title>
    </head>
    <body>
        <div>
            <jsp:include page="templates/header.jsp" flush="true"/>
            Enter new product info:<br/>
            <form action="edit" method="post">
                <table>
                    <input type="hidden" name="id" value="${id}">
                    <tr>
                        <td>Name:</td>
                        <td><input type="text" name="productName" value="${product.name}"></td>
                        <td style="color:red"> ${nameError}</td>
                    </tr>
                    <tr>
                        <td>Price:</td>
                        <td><input type="text" name="price" value="${product.price}"></td>
                        <td style="color:red"> ${priceError}</td>
                    </tr>
                     <tr>
                        <td>Description:</td>
                        <td><input type="text" name="description" value="${product.description}"></td>
                    </tr>                    
                </table>
                <input type="submit" value="Modify" name="action">
                <input type="submit" value="Cancel" name="action">
            </form> 
            <jsp:include page="templates/footer.jsp" flush="true"/>
        </div>
    </body>
</html>
