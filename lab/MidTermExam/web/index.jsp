<%-- 
    Document   : index
    Created on : Jun 4, 2016, 9:57:20 AM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mid-Term Exam</title>
    </head>
    <body>
        <div>
            <h1 style="text-align: center">This is the Midterm Programming Exam</h1>
            To start the web-application please click start.<br/>
            The number of users currently logged in is : ${sessionCount}<br/>
            <br/>
            <a href="login.jsp"><button>Start</button></a>
            <jsp:include page="templates/footer.jsp" flush="true"/>
        </div>
    </body>
</html>
