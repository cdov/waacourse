<%-- 
    Document   : footer
    Created on : Jun 4, 2016, 10:08:18 AM
    Author     : sam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>-->
<div>
<p>Welcome, ${loginUser.name}</p>

<div style="float:right"><a href="<c:url value="/logout"/>"><button>Logout</button></a></div>
</div>