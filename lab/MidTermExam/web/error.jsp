<%-- 
    Document   : index
    Created on : Jun 4, 2016, 9:57:20 AM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mid-Term Exam</title>
    </head>
    <body>
        <div>
            <p style="text-align: center">It looks like something broke :S</p>
            <p style="text-align: center">Please contact our technical support at</p>
            <p style="text-align: center">support@cs545.edu</p>
            <jsp:include page="templates/footer.jsp" flush="true"/>
        </div>
    </body>
</html>
