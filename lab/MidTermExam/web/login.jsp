<%-- 
    Document   : login
    Created on : Jun 4, 2016, 9:59:25 AM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Please log in to continue</h1>
        <form action="login" method="post">
            <table>
                <tr>
                    <td>User Name:</td>
                    <td><input type="text" name="username"></td>
                    <td style="color:red"> ${userError}</td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type="password" name="password"></td>
                    <td style="color:red">
                        ${passwordError}
                    </td>
                </tr>
            </table>
            <input type="submit" value="Login" />
            <div style="color:red">${loginError}</div>
        </form>
        <jsp:include page="templates/footer.jsp" flush="true"/>
    </body>
</html>
