<%-- 
    Document   : index
    Created on : Jun 4, 2016, 9:57:20 AM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mid-Term Exam</title>
    </head>
    <body>
        <div>
            <p style="text-align: center">Oops it looks like the page you requested has disappeared.</p>
            <jsp:include page="templates/footer.jsp" flush="true"/>
        </div>
    </body>
</html>
