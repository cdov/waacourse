<%-- 
    Document   : index
    Created on : Jun 4, 2016, 9:57:20 AM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mid-Term Exam</title>
    </head>
    <body>
        <div>
            Sorry, you are not authorized to view this page<br/>
            Please login first<br/>
            <a href="login.jsp"><button>Go to login page</button></a>
            <jsp:include page="templates/footer.jsp" flush="true"/>
        </div>
    </body>
</html>
