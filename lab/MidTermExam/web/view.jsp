<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>View Student</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <jsp:include page="templates/header.jsp" flush="true"/>
         The list of products in our system are:<br/>
        <form action="home.jsp" method="get">
            <table border="1">
                <tr>
                    <th>Index</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Edit Link</th>
                    <th>Delete Link</th>
                </tr>
                <c:set var="id" value="${1}"/>
                <c:forEach items="${products}" var="product">
                    <tr>
                        <td><c:out value="${id}" /></td>
                        <td><c:out value="${product.name}" /></td>
                        <td><c:out value="$${product.price}" /></td>
                        <td><c:out value="${product.description}" /></td>
                        <td><a href="<c:url value="/edit?id=${id}" />">Edit</a></td>
                        <td><a href="#">Delete</a></td>
                        <c:set var="id" value="${id+1}"/>
                    </tr>
                </c:forEach>
            </table>
            <br>
            <input type="submit" value="Done" name="action">
        </form>
    </body>
</html>
