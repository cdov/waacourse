/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 984897
 */
public class Book {

    private String title;
    private int page;
    private String category;

    public Book() {
        categories.add("Comedy");
        categories.add("Novel");
        categories.add("Science");
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getCategories() {
        return categories;
    }

//    public void setCategories(List<String> categories) {
//        this.categories = categories;
//    }
    private List<String> categories=new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

}
