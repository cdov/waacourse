/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.managedbean;

import edu.mum.cs545.bean.Credential;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author 984897
 */
@Named("authenticateBean")
@SessionScoped
public class AuthenticateLogin implements Serializable {
    
    @Inject
    private Credential credential;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }
    /**
     * Creates a new instance of AuthenticateLogin
     */
    public AuthenticateLogin() {
        
    }
    
    public String authenticate(){
        if(credential.getUsername().equalsIgnoreCase("Sammy") && credential.getPassword().equals("123456")){
            return "main.jsf";
        }
        else{
            this.setError("Login Failed!");
            return "login.jsf";
        }
    }
    
}
