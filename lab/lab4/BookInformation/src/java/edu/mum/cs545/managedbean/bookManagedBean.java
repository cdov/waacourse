/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.managedbean;

import edu.mum.cs545.bean.Book;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author 984897
 */
@Named(value = "bookManagedBean")
@ApplicationScoped
public class bookManagedBean {
    private List<Book> books=new ArrayList<>();

    public List<Book> getBooks() {
        return books;
    }

//    public void setBooks(List<Book> books) {
//        this.books = books;
//    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
    private Book book=new Book();
    /**
     * Creates a new instance of bookManagedBean
     */
    public bookManagedBean() {
    }
    
    public String addBookPage(){
        book=new Book();
        return "add.jsf";
    }
    public String addBook(){
        books.add(book);
        return "main.jsf";
    }
    public String clearBook(){
        books.clear();
        return "main.jsf";
    }
}
