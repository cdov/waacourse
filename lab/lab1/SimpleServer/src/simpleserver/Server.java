/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author csfaculty
 */
public class Server {

    private int port = 8181;
    private int backlog = 5;
    private String bindingAddress = "127.0.0.1";

    public void startup() {
        try {
            ServerSocket serverSocket = new ServerSocket(port, backlog, InetAddress.getByName(bindingAddress));
            System.out.println("Ready");
            Socket socket = serverSocket.accept();
            System.out.println("Accepted");
            InputStream inputStream = socket.getInputStream();

//            int characterRead = 0;
//            while (inputStream.available() != 0) {
//                characterRead = inputStream.read();
//                System.out.print((char) characterRead);
//            }

            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            String route = (in.readLine().split(" "))[1];

            System.out.println(route);
            OutputStream outputStream = socket.getOutputStream();

            switch (route) {
                case "/page1.html":
                    File file = new File(System.getProperty("user.dir")+"/HW1.html");
                    FileInputStream fis = new FileInputStream(file);
                    byte[] data = new byte[(int) file.length()];
                    fis.read(data);
                    fis.close();

                    String str = new String(data, "UTF-8");
                    writeResponse(outputStream, str, "200 OK");
                    break;
                case "/page2.html":
                    writeResponse(outputStream, "23", "200 OK");
                    break;
                case "/page.html":
                    writeResponse(outputStream, "301", "301 Moved Permanently");
                    break;
                default:
                    writeResponse(outputStream, "404", "404 Not Found");

            }

            in.close();
            Thread.sleep(10000);
            socket.close();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeResponse(OutputStream outputStream, String response, String status) {
        try (PrintWriter out = new PrintWriter(outputStream)) {
            out.println("HTTP/1.1 " + status);
            out.println("Content-Type: text/html");
            out.println("Content-Length: " + response.length());
            out.println();
            out.println(response);
            out.println();
            out.flush();
        }
    }
}
