/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.managedbean;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author sam
 */
@Named(value = "pageMB")
@ApplicationScoped
public class PageMB {

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    private int page;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    /**
     * Creates a new instance of PageMB
     */
    public PageMB() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        count = Integer.parseInt(ctx.getExternalContext().getInitParameter("initCount"));
    }

    public String nextPage() {
        count++;
        if (page < 8) {
            page++;
        } else {
            page = 1;
        }
        return "/character/1?faces-redirect=true";
    }

    public String previousPage() {
        count++;
        if (page > 1) {
            page--;
        } else {
            page = 8;
        }
        return "/character/1?faces-redirect=true";
    }

    public String showPage(int page) {
        count++;
        setPage(page);
        return "/character/1?faces-redirect=true";
    }
}
