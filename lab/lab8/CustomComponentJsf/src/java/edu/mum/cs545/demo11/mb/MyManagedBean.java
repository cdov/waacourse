/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.demo11.mb;

import edu.mum.cs545.demo11.bean.MyBean;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author csfaculty
 */
@Named
@RequestScoped
public class MyManagedBean {
    private MyBean myBean= new MyBean();

    public MyBean getMyBean() {
        return myBean;
    }
    
}
