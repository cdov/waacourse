/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.demo11.component;

import java.io.IOException;
import java.util.Map;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author csfaculty
 */
@FacesComponent("cs545.Greeting")
public class MyGreetingComponent extends UIInput {

    private final String SIZE = "size";
    private final String INPUT_NAME = "in";
    private final String BTN_NAME = "btn";

    public MyGreetingComponent() {
        setRendererType(null);
    }

    @Override
    public void decode(FacesContext context) {
        //This reads hhtp request
        Map requestMap = context.getExternalContext().getRequestParameterMap();
        String clientId = getClientId(context);
        String value="";
        int size=0;
        if (requestMap.containsKey(clientId + INPUT_NAME)) {
            value=(String) requestMap.get(clientId + INPUT_NAME);
        }
        if (requestMap.containsKey(clientId + SIZE)) {
            size=toInteger(requestMap.get(clientId + SIZE));
        }
        String name;
        if (size < value.length()) {
            name = value.substring(0, size);
        } else {
            name = value;            
        }
        setSubmittedValue(name);
//        try {
//            int submittedValue
//                    = Integer.parseInt((String) requestMap.get(clientId));
//            int newValue = getIncrementedValue(submittedValue, increment);
//            setSubmittedValue("" + newValue);
//        } catch (NumberFormatException ex) {
//            // let the converter take care of bad input, but we still have
//            // to set the submitted value or the converter won't have
//            // any input to deal with
//            setSubmittedValue((String) requestMap.get(clientId));
//        }
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String clientId = this.getClientId();

        //Write HTML
        encondeLabel(writer, clientId);
        encodeInput(writer, clientId + INPUT_NAME);
        encodeSizeInput(writer, clientId + SIZE);
        encodeButton(writer, clientId + BTN_NAME);
        encodeOutput(writer, clientId);

    }

    private void encondeLabel(ResponseWriter writer, String clientId) throws IOException {
        writer.startElement("H3", this);
        writer.write("Enter Name: ");
        writer.endElement("H3");
    }

    private void encodeInput(ResponseWriter writer, String clientId) throws IOException {
        writer.startElement("input", this);
        writer.writeAttribute("type", "text", null);
        writer.writeAttribute("name", clientId, null);
//        Object value = getValue();
//        if (value != null) {
//            writer.writeAttribute("value", value, null);
//        }
        writer.endElement("input");
    }

    private void encodeSizeInput(ResponseWriter writer, String clientId) throws IOException {
        writer.startElement("H3", this);
        writer.write("Enter Size: ");
        writer.endElement("H3");
        writer.startElement("input", this);
        writer.writeAttribute("type", "text", null);
        writer.writeAttribute("name", clientId, null);
//        Object value = getValue();
//        Object input = getAttributes().get("input");
//        if (value != null) {
//            writer.writeAttribute("value", value, null);
//        }
        writer.endElement("input");
    }

    private void encodeButton(ResponseWriter writer, String clientId) throws IOException {
        writer.startElement("input", this);
        writer.writeAttribute("type", "submit", null);
        writer.writeAttribute("name", clientId, null);
        writer.writeAttribute("value", "Greet", null);
        writer.endElement("input");
    }

    private void encodeOutput(ResponseWriter writer, String clientId) throws IOException {
        writer.startElement("H3", this);
        Object value = this.getValue();        
//        String name = "";
        if (value != null) {
//            int size = toInteger(this.getAttributes().get(SIZE));
//            if (size < value.toString().length()) {
//                name = value.toString().substring(0, size);
//            } else {
//                name = value.toString();
//            }
            writer.write("Hello " + value);
        }else{
            writer.write("Hello ");
        }
        writer.endElement("H3");        
    }

    private int toInteger(Object value) {
        Integer intValue = null;
        if (value instanceof Number) {
            intValue = ((Integer) value);
        } else if (value instanceof String) {
            intValue = Integer.parseInt((String) value);
        } else {
            throw new IllegalArgumentException("Cannot convert " + value + " to number.");
        }
        return intValue;
    }

}
