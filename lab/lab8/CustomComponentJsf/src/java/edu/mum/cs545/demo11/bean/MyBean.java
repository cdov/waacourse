/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs545.demo11.bean;

/**
 *
 * @author csfaculty
 */
public class MyBean {
    private String name= "Jack";
    private String greeting= "Hello from the class of WAA 2016 at MUM.";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
    
}
