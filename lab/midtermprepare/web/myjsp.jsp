<%-- 
    Document   : myjsp
    Created on : Jun 3, 2016, 9:17:55 PM
    Author     : sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="my" uri="/WEB-INF/tlds/myTag" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <my:mytag attr="1" />
        ${my:myfunction()}
    </body>
</html>
